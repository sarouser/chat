const express = require("express")
const app = express();

const http = require('http').Server(app);
const io = require('socket.io')(http);

app.use(express.static("public"));

// app.get('/', (req, res) => {
//     res.sendFile(__dirname + '/index.html');
// });

let allClients = {};
let usedNames = [];


let func = (() => {
    //implementing Open-Closed Principle
    class NumberConverter {
        convertBase(num, from, to) {
            return parseInt(num, +from).toString(+to);
        }
    }

    class DecimalToBinary extends NumberConverter {
        dec2bin(number) {
            return this.convertBase(number, 10, 2);
        }
    }

    class BinaryToDecimal extends NumberConverter{
        bin2dec(num){
            return this.convertBase(num,2,10);
        }
    }
    let counter = 1;
    return (socket) => {
        socket.on("command" , (obj) => {
            let answer;
            console.log("mtav");
            console.log("obj:" , obj.from)
            if (obj.from === 10 && obj.to === 2) {
                console.log(11)
                answer = new DecimalToBinary().dec2bin(obj.num);
            } else if(obj.from === 2 && obj.to === 10){
                console.log(22)
                answer = new BinaryToDecimal().bin2dec(obj.num);
            }
            console.log(answer);
            socket.emit("answer" , answer);
        })
        socket.emit("nickname", `user${counter}`);
        socket.on('disconnect', () => {
            socket.broadcast.emit("user disconnected", --counter);
        });

        socket.on("command", (msg) => {

            socket.broadcast.emit("user disconnected", --counter);
        });

        console.log(`connection emitted:${counter} times`);
        socket.broadcast.emit("user connected", counter++);
        socket.on('chat message', (msg) => {
            io.emit('chat message', msg);
            console.log("emitting chat message!");
        });
    }
})();

io.on('connection', func);

const port = 4000;
http.listen(port, () => {
    console.log(`listening on *:${port}`);
});