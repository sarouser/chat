// alert("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
let socket = io();
let messages = document.getElementById('messages');
let form = document.getElementById('form');
let input = document.getElementById('input');
let nick = document.getElementById("username");

window.addEventListener('DOMContentLoaded', (event) => {
    console.log('DOM fully loaded and parsed');
});

form.addEventListener('submit', function (e) {
    e.preventDefault();
    if (input.value) {
        if (input.value.indexOf("/num,") === 0) {
            let obj = {};
            let inp = input.value.slice(5).split(",");
            console.log("arr")
            console.log(inp)
            obj["num"] = inp[0]
            obj["from"] = parseInt(inp[1],10)
            obj["to"] = parseInt(inp[2],10)
            console.log("obj:" ,obj)
            socket.emit("command", obj);
            input.value = "";
        } else {
            socket.emit('chat message', input.value);
            input.value = '';
        }
    }
});

socket.on("answer", (msg) => {
    let item = document.createElement('li');
    item.textContent = `your answer:${msg}`;
    item.id = "notif";
    messages.appendChild(item);
    window.scrollTo(0, document.body.scrollHeight);
})

socket.on("user disconnected", (msg) => {
    console.log("disconnected msg:", msg)
    let item = document.createElement('li');
    item.textContent = `${msg} user is disconnected!`;
    item.id = "notif";
    messages.appendChild(item);
    window.scrollTo(0, document.body.scrollHeight);
});

socket.on("nickname", (msg) => {
    console.log("socket emit worked")
    let nickname = document.createElement("h3");
    nickname.textContent = msg;
    nick.appendChild(nickname);
});

socket.on('user connected', function (msg) {
    let notif = document.createElement('li');
    notif.textContent = `${msg} user is connected!`;
    notif.id = "notif";
    messages.appendChild(notif);
    window.scrollTo(0, document.body.scrollHeight);
});

socket.on('chat message', function (msg) {
    console.log("got message, including it into the page!")
    var item = document.createElement('li');
    item.textContent = msg;
    messages.appendChild(item);
    window.scrollTo(0, document.body.scrollHeight);
});